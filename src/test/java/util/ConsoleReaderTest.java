package util;

import org.junit.Assert;
import org.junit.Test;
import view.ConsoleView;

import java.util.HashSet;
import java.util.Set;

public class ConsoleReaderTest {
    private ConsoleReader cr = new ConsoleReader(new ConsoleView());
    @Test
    public void checkRangeTest(){
        Set<Integer> key = new HashSet<>();
        key.add(1);
        key.add(2);
        key.add(3);
        Assert.assertTrue(cr.checkRange(2, key));
    }

    @Test(expected = NumberFormatException.class)
    public void checkRangeTest2(){
        Set<Integer> key = new HashSet<>();
        key.add(1);
        key.add(2);
        key.add(3);
        cr.checkRange(5, key);
        System.out.println("sad");
    }


}
