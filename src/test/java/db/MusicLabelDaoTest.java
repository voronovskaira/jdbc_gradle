package db;

import model.MusicLabel;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Map;

public class MusicLabelDaoTest {

    private Dao<MusicLabel> dao = new MusicLabelDao();
    private MusicLabel musicLabel = new MusicLabel("Testing music label");
    private int labelIndex;

    @Test
    public void testGetMethod() {
        try {
            dao.create(musicLabel);
            Map<Integer, String> labels = dao.getAll();
            labelIndex = labels.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue().equals(musicLabel.getName()))
                    .findFirst()
                    .get()
                    .getKey();
            MusicLabel createdLAbel = dao.get(labelIndex);
            Assert.assertEquals(createdLAbel, musicLabel);
            dao.delete(labelIndex);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCreateMethod() {
        try {
            Map<Integer, String> labels = dao.getAll();
            Assert.assertFalse(labels.values().contains(musicLabel.getName()));
            dao.create(musicLabel);
            labels = dao.getAll();
            Assert.assertTrue(labels.values().contains(musicLabel.getName()));
            labelIndex = labels.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue().equals(musicLabel.getName()))
                    .findFirst()
                    .get()
                    .getKey();
            dao.delete(labelIndex);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteMethod() {
        try {
            Map<Integer, String> labels = dao.getAll();
            Assert.assertFalse(labels.values().contains(musicLabel.getName()));
            dao.create(musicLabel);
            labels = dao.getAll();
            Assert.assertTrue(labels.values().contains(musicLabel.getName()));
            labelIndex = labels.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue().equals(musicLabel.getName()))
                    .findFirst()
                    .get()
                    .getKey();
            dao.delete(labelIndex);
            labels = dao.getAll();
            Assert.assertFalse(labels.values().contains(musicLabel.getName()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdateMethod() {
        try {
            dao.create(musicLabel);
            Map<Integer, String> labels = dao.getAll();
            Assert.assertTrue(labels.values().contains(musicLabel.getName()));
            labelIndex = labels.entrySet()
                    .stream()
                    .filter(entry -> entry.getValue().equals(musicLabel.getName()))
                    .findFirst()
                    .get()
                    .getKey();
            MusicLabel label = new MusicLabel("Updated label");
            dao.update(label, labelIndex);
            Assert.assertEquals(dao.get(labelIndex), label);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
