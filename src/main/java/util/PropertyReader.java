package util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view.ConsoleView;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {

    private static String value;
    private static Logger LOG = LogManager.getLogger(PropertyReader.class);


    public static String readProperties(String key) {
        Properties property = new Properties();
        try {
            FileInputStream file = new FileInputStream("src/main/resources/db_config.properties");
            property.load(file);
            value = property.getProperty(key);
        } catch (IOException e) {
            LOG.error("ERROR");
        }
        return value;
    }
}
