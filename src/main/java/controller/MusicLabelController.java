package controller;

import constant.MenuMessages;
import db.Dao;
import db.MusicLabelDao;
import model.MusicLabel;
import util.ConsoleReader;
import util.MenuOptions;
import view.View;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MusicLabelController extends Controller<MusicLabel> {

    private Dao<MusicLabel> dao = new MusicLabelDao();
    private View view;
    private ConsoleReader consoleReader;

    public MusicLabelController(View view, ConsoleReader consoleReader) {
        this.view = view;
        this.consoleReader = consoleReader;
    }

    public MusicLabel get() {
        MusicLabel label = new MusicLabel();
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose music label to view info", map.keySet());
            label = dao.get(userChoice);
        } catch (SQLException e) {
            LOG.error("Couldn't get music label");
        }
        return label;
    }


    public Map<Integer, String> getAll() {
        Map<Integer, String> map = new HashMap<>();
        try {
            map = dao.getAll();
        } catch (SQLException e) {
            System.out.println("Cant create new record");
        }
        return map;
    }


    public MusicLabel create() {
        MusicLabel label = new MusicLabel();
        try {
            label.setName(consoleReader.scanFromConsole(MenuMessages.INPUT_MUSIC_LABEL_NAME));
            dao.create(label);
            view.print(MenuMessages.RECORD_ADDED + label);
        } catch (SQLException e) {
            System.out.println("Cant create new record");
        }
        return label;
    }


    public void update() {
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose music label to update", map.keySet());
            MusicLabel label = new MusicLabel();
            label.setName(consoleReader.scanFromConsole(MenuMessages.INPUT_MUSIC_LABEL_NAME));
            dao.update(label, userChoice);
            view.print(MenuMessages.RECORD_UPDATED);
        } catch (SQLException e) {
            System.out.println("Cant find record");
        }
    }


    public void delete() {
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose music label to delete", map.keySet());
            dao.delete(userChoice);
            view.print(MenuMessages.RECORD_DELETED);
        } catch (SQLException e) {
            System.out.println("cant delete record");
        }
    }

}
