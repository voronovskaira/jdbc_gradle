package controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public abstract class Controller<T> {

    protected static Logger LOG = LogManager.getLogger(Controller.class);

    public abstract T get();

    public abstract T create();

    public abstract Map<Integer, String> getAll();

    public abstract void update();

    public abstract void delete();


}
