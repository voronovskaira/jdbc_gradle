package menu;

import java.util.HashMap;
import java.util.Map;

public class TablesList {

    private static Map<Integer, String> tables = new HashMap<>();

    static {
        tables.put(0, "Quit");
        tables.put(1, "Artist");
        tables.put(2, "Music labels");
        tables.put(3, "Albums");
        tables.put(4, "Songs");
        tables.put(5, "User");
        tables.put(6, "Playlist");
    }

    public static Map<Integer, String> getTablesList() {
        return tables;
    }
}
