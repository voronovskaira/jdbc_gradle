package menu;

import java.util.HashMap;
import java.util.Map;

public class OperationList {

    private static Map<Integer, String> operations = new HashMap<>();

    static {
        operations.put(1, "create");
        operations.put(2, "read");
        operations.put(3, "update");
        operations.put(4, "delete");
    }

    public static Map<Integer, String> getOperations() {
        return operations;
    }

}
