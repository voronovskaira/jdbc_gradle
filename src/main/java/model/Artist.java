package model;

public class Artist {
    private String name;
    private MusicLabel musicLabel;

    public Artist(String name, MusicLabel musicLabel) {
        this.name = name;
        this.musicLabel = musicLabel;
    }

    public Artist(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MusicLabel getMusicLabel() {
        return musicLabel;
    }

    public void setMusicLabel(MusicLabel musicLabel) {
        this.musicLabel = musicLabel;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "name='" + name + '\'' +
                ", musicLabel=" + musicLabel +
                '}';
    }
}
