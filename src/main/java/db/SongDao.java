package db;

import constant.Query;
import model.Song;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class SongDao implements Dao<Song> {
    private Connection connection;
    private AlbumDao albumDao = new AlbumDao();

    @Override
    public Song get(int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.GET_SONG_QUERY);
        statement.setLong(1, id);
        ResultSet rs = statement.executeQuery();
        Song song = new Song();
        while (rs.next()) {
            song.setName(rs.getString("name"));
            song.setPrice(rs.getInt("price"));
            song.setAlbum(albumDao.get(rs.getInt("album")));
        }
        return song;
    }

    @Override
    public Map<Integer, String> getAll() throws SQLException {
        connection = ConnectionManager.getConnection();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(Query.GET_ALL_SONG_QUERY);
        Map<Integer, String> map = new HashMap<>();
        while (rs.next()) {
            map.put(rs.getInt("id"), rs.getString("name"));
        }
        return map;
    }

    @Override
    public void create(Song song) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.ADD_SONG_QUERY);
        statement.setString(1, song.getName());
        statement.setInt(2, albumDao.getId(song.getAlbum()));
        statement.setInt(3, song.getPrice());
        statement.execute();
    }

    @Override
    public void update(Song song, int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.UPDATE_SONG_QUERY);
        statement.setString(1, song.getName());
        statement.setLong(2, id);
        statement.execute();
    }

    @Override
    public void delete(int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.DELETE_SONG_QUERY);
        statement.setLong(1, id);
        statement.execute();
    }
}
