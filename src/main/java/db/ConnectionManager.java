package db;

import util.PropertyReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

public class ConnectionManager {

    private static Connection connection;

    private ConnectionManager() {
        try {
            connection = DriverManager.getConnection(
                    PropertyReader.readProperties("host"),
                    PropertyReader.readProperties("username"),
                    PropertyReader.readProperties("password"));
        } catch (SQLException e) {
            System.out.println("couldn't connect to database, Please check your connection properties");
        }
    }

    public static Connection getConnection() {
        if (Objects.isNull(connection)) {
            new ConnectionManager();
            return connection;
        } else
            return connection;
    }
}
