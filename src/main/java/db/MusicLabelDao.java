package db;

import constant.Query;
import model.MusicLabel;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class MusicLabelDao implements Dao<MusicLabel> {

    private Connection connection;

    @Override
    public MusicLabel get(int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.GET_MUSIC_LABEL_QUERY);
        statement.setLong(1, id);
        ResultSet rs = statement.executeQuery();
        MusicLabel label = new MusicLabel();
        while (rs.next()){
            label.setName(rs.getString("name"));
        }
        return label;
    }

    @Override
    public Map<Integer, String> getAll() throws SQLException {
        connection = ConnectionManager.getConnection();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(Query.GET_ALL_MUSIC_LABELS_QUERY);
        Map<Integer, String> map = new HashMap<>();
        while (rs.next()){
            map.put(rs.getInt("id"), rs.getString("name"));
        }
        return map;
    }

    @Override
    public void create(MusicLabel musicLabel) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.ADD_MUSIC_LABEL_QUERY);
        statement.setString(1, musicLabel.getName());
        statement.execute();
    }

    @Override
    public void update(MusicLabel musicLabel, int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.UPDATE_MUSIC_LABEL_QUERY);
        statement.setString(1, musicLabel.getName());
        statement.setLong(2, id);
        statement.execute();
    }

    @Override
    public void delete(int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.DELETE_MUSIC_LABEL_QUERY);
        statement.setLong(1, id);
        statement.execute();
    }

    public int getLabelId(MusicLabel musicLabel) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.FIND_LABEL_ID_QUERY);
        statement.setString(1, musicLabel.getName());
        ResultSet rs = statement.executeQuery();
        rs.next();
        return rs.getInt("id");
    }
}
